pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.toString() == "kotlin-multiplatform") {
                useModule("org.jetbrains.kotlin:kotlin-gradle-plugin:${requested.version}")
            }
        }
    }
}
rootProject.name = "multiplatlib"


enableFeaturePreview("GRADLE_METADATA")
